import React, { useState } from "react";
import { Responsive, WidthProvider } from "react-grid-layout";
import { v4 as uuidv4 } from 'uuid';
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";

const ResponsiveGridLayout = WidthProvider(Responsive);

export default function ThirdTry() {

    const [layout2, setLayout2] = useState([]);

    const handleDragOver = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = "move";
        event.currentTarget.classList.add("drag-over");
    };

    const handleDragLeave = (event) => {
        event.preventDefault();
        event.currentTarget.classList.remove("drag-over");
    };

    const COMPONENTS = {
        component1: { name: "Componente 1", content: <div>Contenido del componente 1</div> },
        component2: { name: "Componente 2", content: <div>Contenido del componente 2</div> },
        component3: { name: "Componente 3", content: <div>Contenido del componente 3</div> },
        // ...
    };

    const handleDrop = (event) => {
        event.preventDefault();
        event.stopPropagation();
        const id = event.dataTransfer.getData('id');
        console.log("este es el id", id)
        const newLayoutItem = {
            i: uuidv4(),
            w: 4,
            h: 4,
            x: event.clientX - event.currentTarget.getBoundingClientRect().left,
            y: event.clientY - event.currentTarget.getBoundingClientRect().top,
            component: COMPONENTS[id],
            componentId: id,
        };

        const existingItem = layout2.find((item) => item.componentId === id);
        console.log("existingItem: ", existingItem);
        if (existingItem) {
            console.log("existingItem encontrado");
            existingItem.i += "COPY:" + id
            existingItem.h += 4;
            setLayout2([...layout2]);
        }else {
            setLayout2((layout) => [...layout, newLayoutItem]);
        }

        // setLayout2((layout) => [...layout, newLayoutItem]);
        event.currentTarget.classList.remove("drag-over");
    };


    const handleLayoutChange = (layout) => {
        setLayout2(layout);
    };

    return (
        <div className="App">
            <div className="section-1">
                {Object.keys(COMPONENTS).map((id) => (
                    <div key={id} className="box" draggable data-id={id}>
                        {COMPONENTS[id].name}
                    </div>
                ))}
            </div>
            <div
                className="section-2"
                onDrop={handleDrop}
                onDragOver={handleDragOver}
                onDragLeave={handleDragLeave}
            >
                <ResponsiveGridLayout
                    className="layout"
                    layouts={{ lg: layout2 }}
                    breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
                    cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
                    rowHeight={30}
                    onLayoutChange={handleLayoutChange}
                    style={{height:"600px"}}
                >
                    {layout2.map((item) => (
                        <div key={item.i} data-grid={item}>
                            <div className="box">
                                <p>{item.i}</p>
                                <span className="drag-handle">☰</span>
                            </div>
                        </div>
                    ))}
                </ResponsiveGridLayout>
            </div>
        </div>
    );
}
